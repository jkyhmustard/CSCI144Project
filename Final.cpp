/*
 * Jae Kyung Yoon
 * CSCI 144
 * Final Project -Intelligent Traffic Light System-
 * December 11, 2015
 * 
 * =====================================
 * Description:
 * The goal of this project is to simulate an intelligent traffic signal system in which considers randomly generated
 * cars from different directions without having deadlocks, crashes, and unexpected termination.
 * The extra credit portion of this project assignment includes the implementation of experimental data collected and recorded,
 * as well as the desired custom number of cars that can be manipulated through coding.
 * =====================================
 * 
 * =====================================
 * Global Notes:
 * This was designed in a way that all the processes are handled within each of the threads created.
 * Everything can be found inside the 4 directional threads.
 * =====================================
 * 
 * =====================================
 * Instructions:
 * 1. Compile the code by typing the following in the terminal (without the apostrophe):
 * ' g++ Final.cpp -lpthread -o final.exe'
 * ' ./final.exe '
 * 2. Check results
 * 3. Yay!
 * =====================================
 */

#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <queue>
#include <ctime>
#include <cstdio>
#include <unistd.h>

using namespace std;

//Defining the maximum time and # of cars
#define NUM_CARS 10 // This can be edited to change the simulation count
#define MAX_TIME 500 // Total time the simulation will run in seconds

//Global Time Constant
double TotalIntersectTime = 0;

//Implementing a Mutex for each of the directions and the intersection
pthread_mutex_t IntLock;
pthread_mutex_t NLock;
pthread_mutex_t SLock;
pthread_mutex_t ELock;
pthread_mutex_t WLock;

//Implementing a Queue for each of the directions and the intersection
queue<int> Queue_Intersect;
queue<int> Queue_North;
queue<int> Queue_South;
queue<int> Queue_East;
queue<int> Queue_West;


//Data of the car, ID and the Message
struct CarData
{
	int CarID;
	char *message;
};

//North Car
void *NThread(void *Message)
{
	struct CarData *my_data;
	my_data = (struct CarData *) Message;
	
	pthread_mutex_lock(&NLock); //Mutex Lock to Push Queue into North
	
	cout << "****************************" << endl;
	cout << "Car # " << my_data->CarID << " is coming from the North!!!" << endl;
	cout << "****************************" << endl;
	cout << "=========================================" << endl;
	cout << Queue_North.size() << " Cars at the North Street." << endl;
	cout << "This car is on North Queue " << Queue_North.size() + 1 << endl << endl;
	Queue_North.push(my_data->CarID); // Push to North Queue
	
	pthread_mutex_unlock(&NLock); //Unlock Mutex
	
	//Assume 3 seconds from North Street to the Intersection
	sleep(3); 

	
	pthread_mutex_lock(&IntLock); // Mutex Lock for Intersection
	
	cout << Queue_Intersect.size() << " Cars waiting at the intersection." << endl;
	cout << "Car # " << my_data->CarID << " is on Intersection Queue " << Queue_Intersect.size() + 1 << endl;
	cout << "=========================================" << endl << endl;
	Queue_Intersect.push(my_data->CarID); // Push to Intersection Queue
	
	pthread_mutex_unlock(&IntLock); //Unlock Mutex
	
	
	clock_t startTime = clock(); //Counting Total Time Spent at the Intersection
	
	while(true) //Checking 
	{
		if((my_data->CarID == Queue_North.front()) && (my_data->CarID == Queue_Intersect.front()))
			break;
		else
		{
			usleep(5);
			continue;
		}
		
	}
	
	//Time taken to pass the intersection
	sleep(3); 
	
	pthread_mutex_lock(&IntLock); //Safety

	//Calculate intersect wait time here and send to global variable
	clock_t endTime = clock();
	clock_t clockTick = endTime - startTime;
	double timeTaken = clockTick / (double) CLOCKS_PER_SEC;
	TotalIntersectTime += timeTaken;
	
	Queue_North.pop();
	Queue_Intersect.pop();
	
	cout << "****************************" << endl;
	cout << "Car # " << my_data->CarID << " left the intersection" << endl;
	cout << "****************************" << endl;
	
	//sleep(2);
	pthread_mutex_unlock(&IntLock);
}

//South Car
void *SThread(void *Message)
{
	struct CarData *my_data;
	my_data = (struct CarData *) Message;
	
	pthread_mutex_lock(&SLock); //Mutex Lock to Push Queue into South
	
	cout << "****************************" << endl;
	cout << "Car # " << my_data->CarID << " is coming from the South!!!" << endl;
	cout << "****************************" << endl;
	cout << "=========================================" << endl;
	cout << Queue_South.size() << " Cars at the South Street." << endl;
	cout << "This car is on South Queue " << Queue_South.size() + 1 << endl << endl;
	Queue_South.push(my_data->CarID); // Push to South Queue
	
	pthread_mutex_unlock(&SLock); //Unlock Mutex
	
	//Assume 3 seconds from South Street to the Intersection
	sleep(3); 
	
	
	pthread_mutex_lock(&IntLock); // Mutex Lock for Intersection
	
	cout << Queue_Intersect.size() << " Cars waiting at the intersection." << endl;
	cout << "Car # " << my_data->CarID << " is on Intersection Queue " << Queue_Intersect.size() + 1 << endl;
	cout << "=========================================" << endl << endl;
	Queue_Intersect.push(my_data->CarID); // Push to Intersection Queue
	
	pthread_mutex_unlock(&IntLock); //Unlock Mutex
	
	
	clock_t startTime = clock(); //Counting Total Time Spent at the Intersection
	
	while(true)
	{
		if((my_data->CarID == Queue_South.front()) && (my_data->CarID == Queue_Intersect.front()))
		{
			break;
		}
		else
		{
			usleep(5);
			continue;
		}
		
	}
	
	//Time taken to pass the intersection
	sleep(3); 
		
	pthread_mutex_lock(&IntLock); //Safety

	//Calculate intersect wait time here and send to global variable
	clock_t endTime = clock();
	clock_t clockTick = endTime - startTime;
	double timeTaken = clockTick / (double) CLOCKS_PER_SEC;
	TotalIntersectTime += timeTaken;
	
	Queue_South.pop();
	Queue_Intersect.pop();
	
	cout << "****************************" << endl;
	cout << "Car # " << my_data->CarID << " left the intersection" << endl;
	cout << "****************************" << endl << endl;

	pthread_mutex_unlock(&IntLock);
}

//East Car
void *EThread(void *Message)
{
	struct CarData *my_data;
	my_data = (struct CarData *) Message;
	
	pthread_mutex_lock(&ELock); //Mutex Lock to Push Queue into South
	
	cout << "****************************" << endl;
	cout << "Car # " << my_data->CarID << " is coming from the East!!!" << endl;
	cout << "****************************" << endl;
	cout << "=========================================" << endl;
	cout << Queue_East.size() << " Cars at the East Street." << endl;
	cout << "This car is on East Queue " << Queue_East.size() + 1 << endl << endl;
	Queue_East.push(my_data->CarID); // Push to East Queue
	
	pthread_mutex_unlock(&ELock); //Unlock Mutex
	
	//Assume 3 seconds from South Street to the Intersection
	sleep(3); 
	
	
	pthread_mutex_lock(&IntLock); // Mutex Lock for Intersection
	
	cout << Queue_Intersect.size() << " Cars waiting at the intersection." << endl;
	cout << "Car # " << my_data->CarID << " is on Intersection Queue " << Queue_Intersect.size() + 1 << endl;
	cout << "=========================================" << endl << endl;
	Queue_Intersect.push(my_data->CarID); // Push to Intersection Queue
	
	pthread_mutex_unlock(&IntLock); //Unlock Mutex
	
	
	clock_t startTime = clock(); //Counting Total Time Spent at the Intersection
	
	while(true)
	{
		if((my_data->CarID == Queue_East.front()) && (my_data->CarID == Queue_Intersect.front()))
		{
			break;
		}
		else
		{
			usleep(5);
			continue;
		}
		
	}
	
	//Time taken to pass the intersection
	sleep(3); 
		
	pthread_mutex_lock(&IntLock); //Safety
	
	//Calculate intersect wait time here and send to global variable
	clock_t endTime = clock();
	clock_t clockTick = endTime - startTime;
	double timeTaken = clockTick / (double) CLOCKS_PER_SEC;
	TotalIntersectTime += timeTaken;
	
	Queue_East.pop();
	Queue_Intersect.pop();
	
	cout << "****************************" << endl;
	cout << "Car # " << my_data->CarID << " left the intersection" << endl;
	cout << "****************************" << endl << endl;

	pthread_mutex_unlock(&IntLock);
}

//West Car
void *WThread(void *Message)
{
	struct CarData *my_data;
	my_data = (struct CarData *) Message;
	
	pthread_mutex_lock(&WLock); //Mutex Lock to Push Queue into South
	
	cout << "****************************" << endl;
	cout << "Car # " << my_data->CarID << " is coming from the West!!!" << endl;
	cout << "****************************" << endl;
	cout << "=========================================" << endl;
	cout << Queue_West.size() << " Cars at the West Street." << endl;
	cout << "This car is on West Queue " << Queue_West.size() + 1 << endl << endl;
	Queue_West.push(my_data->CarID); // Push to East Queue
	
	pthread_mutex_unlock(&WLock); //Unlock Mutex
	
	//Assume 3 seconds from South Street to the Intersection
	sleep(3); 
	
	
	pthread_mutex_lock(&IntLock); // Mutex Lock for Intersection
	
	cout << Queue_Intersect.size() << " Cars waiting at the intersection." << endl;
	cout << "Car # " << my_data->CarID << " is on Intersection Queue " << Queue_Intersect.size() + 1 << endl;
	cout << "=========================================" << endl << endl;
	Queue_Intersect.push(my_data->CarID); // Push to Intersection Queue
	
	pthread_mutex_unlock(&IntLock); //Unlock Mutex
	
	
	clock_t startTime = clock(); //Counting Total Time Spent at the Intersection
	
	while(true)
	{
		if((my_data->CarID == Queue_West.front()) && (my_data->CarID == Queue_Intersect.front()))
		{
			break;
		}
		else
		{
			usleep(5);
			continue;
		}
		
	}
	
	//Time taken to pass the intersection
	sleep(3); 
		
	pthread_mutex_lock(&IntLock); //Safety
	
	//Calculate intersect wait time here and send to global variable
	clock_t endTime = clock();
	clock_t clockTick = endTime - startTime;
	double timeTaken = clockTick / (double) CLOCKS_PER_SEC;
	TotalIntersectTime += timeTaken;
	
	Queue_West.pop();
	Queue_Intersect.pop();
	
	cout << "****************************" << endl;
	cout << "Car # " << my_data->CarID << " left the intersection" << endl;
	cout << "****************************" << endl << endl;

	pthread_mutex_unlock(&IntLock);
}

//Random Number Generator
int RandN(int min, int max)
{
	int i = 0;
	i = (rand() % (max - min)) + min;
	return i;
}

//Main Program goes here
int main()
{
	pthread_t myThreads[NUM_CARS]; //Making threads
	struct CarData CarD[NUM_CARS]; //Making thread structs
	
	if(pthread_mutex_init(&NLock, NULL) != 0 || pthread_mutex_init(&SLock, NULL) != 0 || pthread_mutex_init(&ELock, NULL) != 0 || pthread_mutex_init(&WLock, NULL) != 0)
	{
		cout << "Mutex Initialization Failed" << endl;
		return 1;
	}
	
	srand(time(0));
	int rand = 0;
	
	for(int i = 0; i < NUM_CARS; i++)
	{
		rand = RandN(0,4);
		
		if(rand == 0)
		{
			CarD[i].CarID = i;
			CarD[i].message = "Car # ";
			pthread_create(&myThreads[i], NULL, NThread, (void *)&CarD[i]);  
		}
		else if(rand == 1)
		{
			CarD[i].CarID = i;
			CarD[i].message = "Car # ";
			pthread_create(&myThreads[i], NULL, SThread, (void *)&CarD[i]);  
		}
		else if(rand == 2)
		{
			CarD[i].CarID = i;
			CarD[i].message = "Car # ";
			pthread_create(&myThreads[i], NULL, EThread, (void *)&CarD[i]);  
		}
		else if(rand == 3)
		{
			CarD[i].CarID = i;
			CarD[i].message = "Car # ";
			pthread_create(&myThreads[i], NULL, WThread, (void *)&CarD[i]);  
		}
		sleep(2); //Generate Cars every 2 seconds
	}
	sleep(20);
	cout << "All Cars have left the intersection safely. Yay!" << endl;
	
	double TotalTime = TotalIntersectTime + NUM_CARS*3;
	
	cout << "=================" << endl;
	cout << "Total Time Taken at the Intersection: " << TotalTime << endl;
	cout << "Total Average Time Taken at the Intersection: " << TotalTime / 9 << endl;
	cout << "=================" << endl;
	
	pthread_mutex_destroy(&IntLock);
	pthread_mutex_destroy(&NLock);
	pthread_mutex_destroy(&SLock);
	pthread_mutex_destroy(&ELock);
	pthread_mutex_destroy(&WLock);
	return 0;
}
